# Monitoren van de vergisting

## Achtergrond
Na het brouwen blijft er wort over. 
Aan deze suiker houdende substantie wordt gist toegevoegd die de suikers om gaat zetten in CO2 en Alcohol. 
Tijdens dit proces vervliegt de CO2 en blijft de alcohol achter in de vloeistof. 
Als gevolg hiervan neemt het soortelijk gewicht af.  

C<sub>6</sub>H<sub>12</sub>O<sub>6</sub> → 2 C<sub>2</sub>H<sub>5</sub>OH + 2 CO<sub>2</sub>   

Nu we wat meer snappen over de chemische veranderingen tijdens de vergisting kunnen we ook gaan bepalen wat we gaan meten: 
1. Het resterende soortelijk gewicht in de wort/jongbier
1. Het alcohol percentage in de wort/jongbier
1. Meet de breking van het licht door de vloeistof
1. De geproduceerde/ontsnappende hoeveelheid CO2 

## Meetmethoden

### Breking van het licht
Met een refractometer kan eenvoudig de breking van het licht door een druppel vloeistof worden gemeten. 
De breking door een vloeistof met veel opgeloste suikers is anders dan door een vloeistof met weinig opgeloste suikers. 
[Refractometers][1] zijn relatief goedkoop en makkelijk in gebruik.

![https://nl.wikipedia.org/wiki/Refractometer](images/200px-Refractometer.jpg)

Het nadeel bij gebruik van een refractometer is dat de waarden af gaan wijken zodra er alcohol in het spel is. 
Deze meetmethode is dus alleen geschikt om de hoeveelheid suiker te bepalen voordat de vergisting van start gaat. 

 
### Gewicht van de vloeistof
Meten hoe zwaar de vloerstof is kan met behulp van een [hydrometer][2]. 
Een hydrometer is een soort dobber die hoger of lager in de vloeistof drijft afhankelijk van de dichtheid 
van de vloeistof. De dichtheid zal veranderen doordat alcohol lichter is dan suiker (immers de c02 ontbreekt). 
100gram suiker opgelost in water en aangevuld met water tot 1 liter zal zwaarder zijn dan 1 liter water zonder oplossingen. 
![2](images/Hydrometer.png)
  
### Alcoholpercentage
Het meten van het alcohol percentage is alleen mogelijk in een lab. Valt daardoor buiten scope van dit project. 

### CO2 Volume
Met een flow meter valt vast te stellen hoeveel gas er ontsnapt uit het gistingsvat. 
Het volume van 1 suikerklontje is heel klein vergeleken met het volume van de alcohol en Co2 die hetzelfde suikerklont 
bij vergisting opleverd. Het is vooral de Co2 in gasvorm die voor het volume zorgt.   

## Opties 
Op de hobbybrouw schaal zijn er een aantal opties verkrijgbaar. 
Een daarvan meet de hoeveelheid gas die het gistingvat verlaat, de overige 2 zijn digitale hydrometers. 

### Plaato
De [plaato][5] plaats je op de plaats van het waterslot. 
Deze sensor meet de hoeveelheid ontsnapte CO2 en de bijgeleverde software probeerd op basis van algoritmen te schatten 
hoeveel suiker er nog in de wort zit. De gemeten waarden kunnen afhankelijk van de luchtdruk, temperatuur en de 
sluiting van het gistingsvat.
![5](images/PlaatoAirlock.jpg)  

### Tilt
De [Tilt][3] werk op basis van het hydrometer principe. Het is een dobber met ingebouwde temperatuur en hellingsensor. 
Aan het begin van de vergisting drijft de Tilt hoger op het wort dan tegen het einde wanneer de Tilt dieper ligt doordat 
de meeste suikers omgezet zijn in alcohol en co2.   
Metingen in het gewicht kunnen afwijken onder invloed van de temperatuur, 
maar met de temperatuur sensor is voor deze afwijking te corrigeren.  
De Tilt communiceerd via bluetooth, daardoor dient er altijd een telefoon, ipad of RaspberryPi in de buurt te zijn.

![3](images/Tilt-Red.png)
    
### iSpindel
De [iSpindel][4] is het DIY antwoord van de brouwgemeenschap op de Tilt. Het principe is hetzelfde, de opzet alleen net iets
anders. De iSpindel communiceerd via een wifi verbinding met het netwerk om daar zijn gemeten waarden ergens naar toe 
te sturen. 

[iSpindel op Hackaday.com][8]

![4](images/ispindel.png)
## Pro's & con's

|              	| Plaato 	| iSpindel 	| Tilt                   	|
|--------------	|--------	|----------	|------------------------	|
| Prijs        	| €145   	| ~€35     	| €165                   	|
| Licentie      | Proprietary | Open source  | Proprietary | 
| Connectivity 	| WiFi   	| WiFi     	| Bluetooth (BLE) 	        |
| Precisie     	| Slecht 	| Goed     	| Goed                   	|
| Uitstraling   | Profi     | Hobby     | Profi                     |
| Waar te koop  | [Link][6] | Zelf samenstellen | [Link][7]         |
| Verborgen kosten | Unknown | | RaspberryPi [^1]

[^1]: RaspberryPi om de bluetooth berichten op te vangen.  
## Conclusie
De resultaten van het project moeten ook getoond kunnen worden aan relaties, mede daarom is ook de professionele 
uitstraling een zwaarwegende factor in de overweging voor welke sensor we zouden moeten kiezen. Qua presentatie oogt 
de Plaato veruit het best, echter door de mindere kwaliteit van de metingen is mijn advies om te kiezen voor de Tilt. 
De RaspberryPi die hierbij nodig is kan desgewenst uit het zicht worden geplaatst. 

De iSpindel is leuk als alternatief wanneer we de resultaten direct naar een eigen endpoint zouden willen posten. Maar 
de hobby uitstraling hiervan maakt het een minder interessante keuze. 

[1]: https://nl.wikipedia.org/wiki/Refractometer
[2]: https://nl.wikipedia.org/wiki/Hydrometer
[3]: https://tilthydrometer.com/products/copy-of-tilt-floating-wireless-hydrometer-and-thermometer-for-brewing
[4]: http://www.ispindel.de/
[5]: https://plaato.io/products/plaato-airlock
[6]: https://www.brouwstore.nl/plaato-digitaal-waterslot
[7]: https://www.brouwstore.nl/tilt-draadloze-hydrometer-en-thermometer
[8]: https://hackaday.com/2017/03/01/iot-device-pulls-its-weight-in-home-brewing/ 
